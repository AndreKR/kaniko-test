FROM alpine:3.9 AS builder
RUN mkdir -p /opt/foo/bar
RUN touch /opt/foo/bar/a
RUN ln -s a /opt/foo/bar/s

FROM alpine:3.9
COPY --from=builder /opt/foo/bar /opt/baz/
